CONTAINER_NAME=gcc-container

echo "stopping previous container"
docker stop $CONTAINER_NAME || true
echo "building new container"
docker build -t $CONTAINER_NAME .
echo "running new container"
docker run -i --rm -P --name $CONTAINER_NAME $CONTAINER_NAME 

